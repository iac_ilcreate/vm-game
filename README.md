# vm-game

This repo is a cloud decision for extra exercise, but you can used it as example infrastructure as code for your needs.

## Getting started

# Global

This repo is a infrastructure code which you can deploy on your cloud resources.
In this case, target cloud provider is [Yandex Cloud](https://yandex.cloud/en/).

## How to use?

```shell
git clone https://oauth2:${YOUR_GITLAB_TOKEN}@gitlab.com/iac_ilcreate/vm-game.git
```

Well, all you need to do is paste 3 your unique variable from Yandex Cloud in terraform.tfvars.

```
service_account_key_file = "value"

cloud_id = "value"

folder_id = "value"
```
These values you can take from [yc-cli](https://yandex.cloud/en/docs/cli/).

Next step, you need to generate service-account in YC. You can use either [yc-cli](https://yandex.cloud/en/docs/cli/).

In results, you have a "key.json", which needed for creating infrastructure in the cloud.

---

After you added needed values, you have to run:

```shell
terraform plan
```

If your planned infrastructure is ok, then run:

```shell
terraform apply
```

Done.

## Quantity of servers.

If you need to scale up node-pull your hosts, you can use **variables.tf**

```
variable "vm" {
  type = list
  description = "list of type servers"
  default = [
  {
    type = "app1"
  },
  {
    type = "lb1"
  }
  ]
}
```

You add a some hosts to array default, for example: **{ type = "web1" }** , after apply configuration will be create new server with suffix web1.
Quantity of servers increased by length of array the variable "vm". The more "type", the more quantity servers.


## Others.
This iac you can use as whatever and wherever. also you can upgrade this iac, if you need.