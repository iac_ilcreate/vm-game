terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.114.0"
    }
  }
  # TO DO
  # to keep a tfstate in the s3 bucket.
  # backend "s3" {
  #   endpoint  = "storage.yandexcloud.net"
  #   bucket    = "terraform.state"
  #   key       = "vm-game/terraform.tfstate"
  #   region    = "ru-central1"

  #   skip_region_validation      = true
  #   skip_credentials_validation = true
  # }
}

provider "yandex" {
  service_account_key_file  = var.service_account_key_file
  cloud_id                  = var.cloud_id
  folder_id                 = var.folder_id
  zone                      = "ru-central1-a"
}

