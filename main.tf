resource "yandex_compute_instance" "vm" {
    depends_on = [ yandex_compute_disk.boot ]
    count       = length(var.vm)
    name        = "game-${var.vm[count.index].type}"
    platform_id = "standard-v1"
    zone        = "ru-central1-a"
    folder_id   = data.yandex_resourcemanager_folder.dev.id
    metadata    = {
      ssh-keys  = "${var.user}:${file("~/.ssh/id_rsa.pub")}"
      type      = var.vm[count.index].type
    }

    resources {
      cores     = 2
      memory    = 2
    }

    dynamic "network_interface" {
      for_each = substr(var.vm[count.index].type, 0, 2) == "lb" ? [1] : []
       content {
        subnet_id = yandex_vpc_subnet.dev.id
        nat       = true
      }
    }

    dynamic "network_interface" {
      for_each = substr(var.vm[count.index].type, 0, 3) == "app" ? [1] : []
      content {
        subnet_id = yandex_vpc_subnet.dev.id
        nat       = false
      }
    }

    boot_disk {
      disk_id   = yandex_compute_disk.boot[count.index].id
    }

    # connection {
    #   host = self.network_interface.0.nat_ip_address
    #   type = "ssh"
    #   user = var.user
    #   private_key = file(var.ssh_private)
    # }

    # provisioner "remote-exec" {
    #   when = create
    #   inline = [ 
    #     "sudo apt-add-repository ppa:ansible/ansible -y",
    #     "sudo apt update",
    #     "sudo apt install python3 -y",
    #     "sudo apt install ansible -y"
    #    ]
    # }
}

resource "yandex_vpc_gateway" "nat-dev" {
  name          = "nat-dev"
  folder_id     = data.yandex_resourcemanager_folder.dev.id
  description   = "this gate is for one way to get access on virtual hosts"
  shared_egress_gateway {
  }
}

resource "yandex_vpc_route_table" "game-rt-dev" {
  network_id    = data.yandex_vpc_network.dev.id
  name          = "game-rt-dev"
  folder_id     = data.yandex_resourcemanager_folder.dev.id
  static_route {
    destination_prefix  = "0.0.0.0/0"
    gateway_id          = yandex_vpc_gateway.nat-dev.id
  }
}

resource "yandex_compute_disk" "boot" {
  count         = length(var.vm)
  name          = "boot-disk-${var.vm[count.index].type}"
  type          = "network-ssd-nonreplicated"
  zone          = "ru-central1-a"
  image_id      = "fd87tirk5i8vitv9uuo1" #ubuntu-20-04-lts-v20220606
  size          = 93
}

data "yandex_vpc_network" "dev" {
  name          = "develop"
  folder_id     = data.yandex_resourcemanager_folder.dev.id
}
resource "yandex_vpc_subnet" "dev" {
  name            = "vpc-dev"
  network_id      = data.yandex_vpc_network.dev.id
  zone            = "ru-central1-a"
  v4_cidr_blocks  = [ "10.20.60.16/28" ]
  folder_id       = data.yandex_resourcemanager_folder.dev.id
  route_table_id  = yandex_vpc_route_table.game-rt-dev.id
}

data "yandex_resourcemanager_folder" "dev" {
  name          = "develop"
}

locals {
  app_instances = [ for instance in yandex_compute_instance.vm : instance.network_interface.0.ip_address if substr(instance.metadata.type, 0, 3) == "app" ]

  lb_instances = [ for instance in yandex_compute_instance.vm : instance.network_interface.0.nat_ip_address if substr(instance.metadata.type, 0, 2) == "lb" ]
}

resource "local_file" "inventory" {
  filename        = "${path.module}/inventory.yml"
  file_permission = "0644"
  content         = yamlencode({
    all = {
      children = {
        app = {
          hosts = {
            for i, ip in local.app_instances : "app${i + 1}" => { ansible_host = ip }
          }
        }
        lb = {
          hosts = {
            for i, ip in local.lb_instances : "lb${i + 1}" => { ansible_host = ip }
          }
        }
      }
    vars = {
        ansible_ssh_private_key_file = var.ssh_private
        ansible_ssh_common_args = "-o ProxyCommand=\"ssh -W %h:%p -q ${var.user}@${local.lb_instances[0]}\""
        ansible_ssh_extra_args="-o StrictHostKeyChecking=no"
        ansible_ssh_timeout = 30
        ansible_port = 22
      }
    }
  })
}

resource "null_resource" "run_ansible" {
  depends_on = [ local_file.inventory ]
  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    command = <<-EOT
        ssh-keyscan -H ${local.lb_instances[0]} > ~/.ssh/known_hosts
        ssh-keyscan -H ${join(",", concat(local.app_instances))} >> ~/.ssh/known_hosts
        sleep 5
        ansible-playbook -v -u ${var.user} --private-key ${var.ssh_private} -i ${path.module}/inventory.yml ${path.module}/ansible/main.yml
    EOT
  }
}