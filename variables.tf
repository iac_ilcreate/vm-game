variable "service_account_key_file" {
  type = string
  default = ""
}

variable "cloud_id" {
  type = string
  default = ""
}

variable "folder_id" {
  type = string
  default = ""
}

variable "ssh_private" {
  type = string
  default = "~/.ssh/id_rsa"
}

variable "vm" {
  type = list
  description = "list of type servers"
  default = [
  {
    type = "app1"
  },
  {
    type = "app2"
  },
  {
    type = "app3"
  },
  {
    type = "lb1"
  }
  ]
}

variable "user" {
  type = string
  default = "ubuntu"
}
