# output "name_n_ext_ip" {
#   value = zipmap(yandex_compute_instance.vm.*.name, yandex_compute_instance.vm[*].network_interface.0.nat_ip_address)
# }

# output "name_n_int_ip" {
#   value = zipmap(yandex_compute_instance.vm.*.name, yandex_compute_instance.vm.*.network_interface.0.ip_address)
# }

output "app_instances" {
  value = local.app_instances
}

output "lb_instances" {
  value = local.lb_instances
}