# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.5.1"
  hashes = [
    "h1:/GAVA/xheGQcbOZEq0qxANOg+KVLCA7Wv8qluxhTjhU=",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.2"
  hashes = [
    "h1:IMVAUHKoydFrlPrl9OzasDnw/8ntZFerCC9iXw1rXQY=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.114.0"
  constraints = "0.114.0"
  hashes = [
    "h1:oye82KhD30jTen6EJNDMY9wPypAZNRSUFi0RHXpJWXY=",
  ]
}
